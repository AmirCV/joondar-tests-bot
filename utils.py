from telegram.ext import Updater
from telegram.ext import MessageHandler, Filters
import telegram
import random
import string
from tinydb import TinyDB
from tinydb import where
import jdatetime

db = TinyDB("./database.json", encoding="utf8")
users = db.table("users")
questions = db.table("questions")
tips = db.table("tips")
'''
users:
chat_id - date - name - action - questions - answers

question:
id - date - from - type - grade - lesson - season - attach id - attach url - text - true answer -  answers - replies

message
from - date - question - reply to - text - attachment

answer
from - answer
'''

welcome_message = """
سلام.
فعلا فقط سوال تصویری پشتیبانی میشه، سوالو طوری که گزینه هاش معلوم باشه بفرستید، بعدم درس و فصلشو انتخاب کنید که مرتب باشه برای مرور قبل کنکور.
سوالام توی کانال @joondar_tests فرستاده میشه
فرستنده سوال و کسایی که اشتباه جواب بدن اسمشون نشون داده نمیشه ولی کسایی که درست جواب بدن نشون داده میشه :/
اگه هم از سوالی خوشتون اومد لایکش کنید بعدا یه سری چیزا اضافه میکنم واسه اکسپلور
"""

categories = {
    "دوازدهم": {
            "1": ("شیمی", {"1": "فصل اول: مولکول ها در خدمت تندرستی", "2": "فصل دوم: آسایش و رفاه در سایه شیمی",
                           "3": "فصل سوم: شیمی، جلوه ای از هنر، زیبایی و ماندگاری",
                           "4": "فصل چهارم: شیمی، راهی به سوی آینده روشن تر"}),
            "2": ("حسابان", {"1": "فصل اول: تابع", "2": "فصل دوم: مثلثات", "3": "فصل سوم: حدهای متناهی و نامتناهی",
                             "4": "فصل چهارم: مشتق", "5": "فصل پنجم: کابرد مشتق"}),
            "3": ("گسسته", {"1": "فصل اول: آشنایی با نظریه اعداد", "2": "فصل دوم: گراف و مدل سازی",
                            "3": "فصل سوم: ترکیبیات"}),
            "4": ("هندسه", {"1": "فصل اول: ماتریس و کاربردها", "2": "فصل دوم: آشنایی با مقاطع مخروطی",
                            "3": "فصل سوم: بردارها"}),
            "5": ("فیزیک", {"1": "فصل اول: حرکت بر خط راست", "2": "فصل دوم: دینامیک و حرکت دایره ای",
                            "3": "فصل سوم: نوسان و موج", "4": "فصل چهارم: بر هم کنش های موج",
                            "5": "فصل پنجم: آشنایی با فیزیک اتمی", "6": "فصل ششم: آشنایی با فیزیک هسته ای"}),
            "6": ("فارسی", {"1": "قرابت", "2": "آرایه", "3": "دستور زبان", "4": "لغت"}),
            "7": ("عربی", {"1": "معنی", "2": "قوائد", "3": "ضبط حرکات"}),
            "8": ("زبان", {"1": "زبان دیگه :/"})
        }
}

broadcast_chats = ["-1001161376910", "-1001408849969"]


def keyboard(name="none", k_type="reply", data=None):
    keyboards = {
        "blank": [["new question"]],
        "4-choice": [["1", "2", "3", "4"], ["cancel"]],
        "caption": [["SKIP"], ["cancel"]],
        "cancel": [["cancel"]]
    }
    if name == "none":
        return telegram.ReplyKeyboardRemove()
    if k_type == "reply":
        if name.startswith("c-"):
            k = [[":/"]]
            if name == "c-0":
                k = categories["دوازدهم"].values()
                k = [[a] for a, b in k]
            elif name.startswith("c-0-"):
                name = name.replace("c-0-", "")
                k = categories["دوازدهم"][name][1]
                k = [[b] for a, b in k.items()]
            return telegram.ReplyKeyboardMarkup(k, resize_keyboard=True, one_time_keyboard=True)
        else:
            return telegram.ReplyKeyboardMarkup(keyboards[name], resize_keyboard=True, one_time_keyboard=True)

    elif k_type == "inline":
        if name == "gqa":
            qid = str(data[0])
            k = [[telegram.InlineKeyboardButton("1", callback_data=qid + '-1'),
                  telegram.InlineKeyboardButton("2", callback_data=qid + '-2'),
                  telegram.InlineKeyboardButton("3", callback_data=qid + '-3'),
                  telegram.InlineKeyboardButton("4", callback_data=qid + '-4')],
                 [telegram.InlineKeyboardButton(str(data[1]) + " 👍", callback_data=qid + '-like'),
                  telegram.InlineKeyboardButton(str(data[2]) + " 👎", callback_data=qid + '-dislike')]]
            return telegram.InlineKeyboardMarkup(k)
        if name == "gta":
            qid = str(data)
            k = [
                [telegram.InlineKeyboardButton("شیمی", callback_data=qid + '-1'),
                 telegram.InlineKeyboardButton("فیزیک", callback_data=qid + '-2'),
                 telegram.InlineKeyboardButton("حسابان", callback_data=qid + '-3')],
                [telegram.InlineKeyboardButton("گسسته", callback_data=qid + '-1'),
                 telegram.InlineKeyboardButton("آمار", callback_data=qid + '-2'),
                 telegram.InlineKeyboardButton("هندسه", callback_data=qid + '-3')],
                [telegram.InlineKeyboardButton("فارسی", callback_data=qid + '-1'),
                 telegram.InlineKeyboardButton("دینی", callback_data=qid + '-2'),
                 telegram.InlineKeyboardButton("عربی", callback_data=qid + '-3')],
                [telegram.InlineKeyboardButton("دهم", callback_data=qid + '-1'),
                 telegram.InlineKeyboardButton("یازدهم", callback_data=qid + '-2'),
                 telegram.InlineKeyboardButton("دوازدهم", callback_data=qid + '-3')]
            ]
            return telegram.InlineKeyboardMarkup(k)
    return telegram.ReplyKeyboardRemove()


def get_date():
    return jdatetime.date.today().strftime("%Y-%m-%d")


def gen_id(database="questions"):
    while True:
        cid = ''.join(random.choice(string.ascii_lowercase) for _ in range(5))
        if database == "questions":
            if questions.contains(where("id") == cid):
                continue

        return cid


def get_user(update, context=None):
    chat_id = update.effective_chat.id
    if not users.contains(where("chat-id") == chat_id):
        users.insert({"chat-id": chat_id, "name": "user",
                      "action": "set_name", "temp": {}, "questions": [],
                      "answers": [], "tips": []})
        context.bot.send_message(chat_id=chat_id, text="Hi, what's your name?")
        return chat_id, None
    user = users.get(where("chat-id") == chat_id)
    return chat_id, user


def update_user(user):
    users.update(user, where("chat-id") == user["chat-id"])


def update_question(question, qid):
    questions.update(question, doc_ids=[qid])


def blank(update: telegram.Update, context: telegram.ext.CallbackContext):
    chat_id, user = get_user(update)
    context.bot.send_message(chat_id=chat_id, text="any questions?", reply_markup=keyboard("blank"))
    user["action"] = "blank_answer"
    user["temp"] = {}
    update_user(user)


def blank_answer(update: telegram.Update, context: telegram.ext.CallbackContext):
    chat_id, user = get_user(update)
    message = update.message.text
    if message == "new question":
        user["action"] = "add_question"
        context.bot.send_message(chat_id=chat_id, text="Ok, send it!", reply_markup=keyboard('cancel'))
    elif message == "new tip":
        user["action"] = "add_tip"
        context.bot.send_message(chat_id=chat_id, text="Ok, send it!", reply_markup=keyboard('cancel'))
    else:
        context.bot.send_message(chat_id=chat_id, text="what? :/\njust select from the menu!", reply_markup=keyboard('blank'))
    update_user(user)


def set_name(update: telegram.Update, context: telegram.ext.CallbackContext):
    chat_id, user = get_user(update)
    user["name"] = update.message.text
    user["action"] = "blank"
    update_user(user)
    context.bot.send_message(chat_id=chat_id, text="All done!")
    context.bot.send_message(chat_id=chat_id, text=welcome_message)
    blank(update, context)


def add_question(update: telegram.Update, context: telegram.ext.CallbackContext):
    chat_id, user = get_user(update)
    photos = update.message.photo
    if len(photos) > 0:
        photo_id = photos[-1].file_id
        photo_url = photos[-1].get_file().file_path
        caption = update.message.caption
        caption = "" if caption is None else caption
        cid = gen_id("questions")
        q_id = questions.insert({"id": cid, "from": user.doc_id, "text": caption,
                                 "type": "photo", "date": get_date(),
                                 "att-id": photo_id, "att-url": photo_url,
                                 "answer": 0, "answers": [], "grade": "دوازدهم", "lesson": "", "season": "",
                                 "likes": [], "dislikes": []})
        user["action"] = "get_lesson"
        user["temp"] = {"id": q_id, "caption": caption != ""}
        update_user(user)
        context.bot.send_message(chat_id=chat_id, text="Select", reply_markup=keyboard("c-0"))


def get_lesson(update: telegram.Update, context: telegram.ext.CallbackContext):
    chat_id, user = get_user(update)
    message = update.message.text
    k = categories["دوازدهم"].values()
    ki = list(categories["دوازدهم"].keys())
    k = [a for a, b in k]
    if message in k:
        q = questions.get(doc_id=user["temp"]["id"])
        lid = k.index(message)
        q["lesson"] = ki[lid]
        update_question(q, user["temp"]["id"])
        user["action"] = "get_season"
        update_user(user)
        context.bot.send_message(chat_id=chat_id, text="Select again :/", reply_markup=keyboard("c-0-" + ki[lid]))
    else:
        context.bot.send_message(chat_id=chat_id, text="just \"SELECT\"", reply_markup=keyboard("c-0"))


def get_season(update: telegram.Update, context: telegram.ext.CallbackContext):
    chat_id, user = get_user(update)
    message = update.message.text
    q = questions.get(doc_id=user["temp"]["id"])
    k = list(categories["دوازدهم"][q["lesson"]][1].values())
    ki = list(categories["دوازدهم"][q["lesson"]][1].keys())
    if message in k:
        lid = k.index(message)
        q["season"] = ki[lid]
        update_question(q, user["temp"]["id"])
        user["action"] = "get_answer"
        update_user(user)
        context.bot.send_message(chat_id=chat_id, text="OK, now send the correct answer", reply_markup=keyboard("4-choice"))
    else:
        context.bot.send_message(chat_id=chat_id, text="just \"SELECT\"", reply_markup=keyboard("c-0-" + q["lesson"]))


def get_answer(update: telegram.Update, context: telegram.ext.CallbackContext):
    chat_id, user = get_user(update)
    message = update.message.text
    try:
        message = int(message)
        assert 0 < message < 5
        q = questions.get(doc_id=user["temp"]["id"])
        q["answer"] = message
        update_question(q, user["temp"]["id"])
        if user["temp"]["caption"]:
            update_user(user)
            question_done(update, context)
        else:
            user["action"] = "question_caption"
            context.bot.send_message(chat_id=chat_id,
                                     text="If you want to add caption, send it, or click on the button below",
                                     reply_markup=keyboard("caption"))
            update_user(user)
    except Exception as e:
        context.bot.send_message(chat_id=chat_id, text=str(e))
        context.bot.send_message(chat_id=chat_id, text="it's not valid :/", reply_markup=keyboard("4-choice"))


def question_caption(update: telegram.Update, context: telegram.ext.CallbackContext):
    chat_id, user = get_user(update)
    try:
        caption = update.message.text
        if not caption == "SKIP":
            questions.update({"text": caption}, doc_ids=[user["temp"]["id"]])
            question_done(update, context)
        else:
            question_done(update, context)
    except Exception as e:
        context.bot.send_message(chat_id=chat_id, text=str(e))
        context.bot.send_message(chat_id=chat_id, text="invalid data :/")


def get_q_text(question):
    text = question["text"]
    text += "\n\n #دوازدهم"
    text += " #" + categories["دوازدهم"][question["lesson"]][0].replace(":", " ").replace(" ", "_")
    text += " #" + categories["دوازدهم"][question["lesson"]][1][question["season"]].replace(":", " ").replace(" ", "_")
    text += " #" + question["id"]
    text += "\n➖➖➖➖➖➖➖➖➖➖➖➖➖➖➖\n\n"
    text += "جواب درست دهنده ها: "
    for u in question["answers"]:
        if u["answer"] == str(question["answer"]):
            u = users.get(doc_id=u["from"])
            text += u["name"] + "، "
    text += "\n\n" + "تعداد پاسخهای غلط: " + str(sum([a["answer"] != str(question["answer"]) for a in question["answers"]]))
    return text


def question_done(update: telegram.Update, context: telegram.ext.CallbackContext):
    chat_id, user = get_user(update)
    context.bot.send_message(chat_id=chat_id, text="done!")
    q = questions.get(doc_id=user["temp"]["id"])
    if q["type"] == "photo":
        text = get_q_text(q)
        for c in broadcast_chats:
            context.bot.send_photo(chat_id=int(c), photo=q["att-id"], caption=text,
                                   reply_markup=keyboard("gqa", "inline", ["gqa-" + str(user["temp"]["id"]), 0, 0]))
            if c == "-1001161376910":
                context.bot.send_message(chat_id=int(c), text="Comments for #" + q["id"])
    blank(update, context)


def add_tip(update: telegram.Update, context: telegram.ext.CallbackContext):
    chat_id, user = get_user(update)
    photos = update.message.photo
    if len(photos) > 0:
        photo_id = photos[-1].file_id
        photo_url = photos[-1].get_file().file_path
        caption = update.message.caption
        caption = "" if caption is None else caption
        cid = gen_id("questions")
        tid = tips.insert({"id": cid, "from": chat_id, "text": caption,
                           "type": "photo", "date": get_date(),
                           "att-id": photo_id, "att-url": photo_url,
                           "comments": {}, "likes": [], "dislikes": []})

        if caption == "":
            user["action"] = "tip_caption"
            user["temp"] = {"id": tid}
            update_user(user)
            context.bot.send_message(chat_id=chat_id, text="send caption")
        else:
            context.bot.send_message(chat_id=chat_id, text="Done!")
            blank(update, context)


def tip_caption(update: telegram.Update, context: telegram.ext.CallbackContext):
    chat_id, user = get_user(update)
    try:
        caption = update.message.text
        tips.update({"text": caption}, doc_ids=[user["temp"]["id"]])
        context.bot.send_message(chat_id=chat_id, text="done!")
        blank(update, context)
    except:
        context.bot.send_message(chat_id=chat_id, text="invalid data :/")


def callback(update: telegram.Update, context: telegram.ext.CallbackContext):
    chat_id = update.callback_query.from_user.id
    user = users.get(where("chat-id") == int(chat_id))
    query = update.callback_query
    if user is None:
        query.answer("You are not a member of @joondar_tests_bot", show_alert=True)
    t, qid, arg = query.data.split("-")
    q = questions.get(doc_id=int(qid))
    true_ans = q["answer"]
    if query.message.caption is not None:
        if t == "gqa":
            if user.doc_id == q["from"]:
                query.answer("It's your own question :)\nAnswer: {}".format(q["answer"]), show_alert=True)
                return
            if arg == "like":
                if user.doc_id in q["likes"]:
                    query.answer("ok :/")
                else:
                    if user.doc_id in q["dislikes"]:
                        q["dislikes"].remove(user.doc_id)
                    q["likes"].append(user.doc_id)
                    update_question(q, int(qid))
                    query.answer()
                    query.edit_message_caption(query.message.caption,
                                               reply_markup=keyboard("gqa", "inline",
                                                                 ["gqa-" + str(qid), len(q["likes"]),
                                                                  len(q["dislikes"])]))
            elif arg == "dislike":
                if user.doc_id in q["dislikes"]:
                    query.answer("ok :/")
                else:
                    if user.doc_id in q["likes"]:
                        q["likes"].remove(user.doc_id)
                    q["dislikes"].append(user.doc_id)
                    update_question(q, int(qid))
                    query.answer()
                    query.edit_message_caption(get_q_text(q),
                                               reply_markup=keyboard("gqa", "inline",
                                                                 ["gqa-" + str(qid), len(q["likes"]),
                                                                  len(q["dislikes"])]))
            else:
                if user.doc_id in [a["from"] for a in q["answers"]]:
                    query.answer(":/\nAnswer: {}".format(q["answer"]))
                else:
                    q["answers"].append({"from": user.doc_id, "answer": arg})
                    update_question(q, int(qid))
                    if arg == str(true_ans):
                        query.answer("✅ true")
                    else:
                        query.answer("❌ noob :/\ncorrect answer is {}".format(true_ans), show_alert=True)
                    query.edit_message_caption(get_q_text(q),
                                               reply_markup=keyboard("gqa", "inline", ["gqa-" + str(qid),
                                                                                       len(q["likes"]),
                                                                                       len(q["dislikes"])]))

