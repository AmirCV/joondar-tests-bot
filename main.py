
from telegram.ext import Updater
from telegram.ext import MessageHandler, Filters, CallbackQueryHandler
import telegram
import random
import string
from tinydb import TinyDB
from tinydb import where
import utils

updater = Updater(token='1556433118:AAEGFOZYawepSqc9zYD_utaH4gBf_cgCLQ0', use_context=True)
dispatcher = updater.dispatcher


def echo(update: telegram.Update, context: telegram.ext.CallbackContext):
    print("update")
    print(update)
    print(update.effective_chat.id)
    if update.effective_chat.id == update.effective_user.id:
        chat_id, user = utils.get_user(update, context)

        if user is None:
            return
        if len(update.message.photo) == 0 and update.message.text == "cancel":
            utils.blank(update, context)
        else:
            func = getattr(utils, user["action"])
            func(update, context)



def callback(update: telegram.Update, context: telegram.ext.CallbackContext):
    utils.callback(update, context)


echo_handler = MessageHandler(Filters.all, echo)
dispatcher.add_handler(CallbackQueryHandler(callback))
dispatcher.add_handler(echo_handler)


updater.start_polling(poll_interval=0.5)
